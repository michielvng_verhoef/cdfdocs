#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing


TAG=`git rev-parse --short HEAD`

docker build -t registry.gitlab.com/commondatafactory/cdfdocs:$TAG$1 .
docker push registry.gitlab.com/commondatafactory/cdfdocs:$TAG$1

cd ./helm

helm upgrade cdfdocsacc . -n cdf-acc --values values.acc.yaml --set-string image.tag=$TAG$1

# see progress
kubectl -n cdf-acc get pods -w
