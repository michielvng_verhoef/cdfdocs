---
title: "Introductie"
path: "/over"
---

 Common Data Factory is in opdracht van <a href="https://www.vngrealisatie.nl/">VNG Realisatie</a> en volgt de Common Ground visie.
      
Naast onze dashboards en viewers biedt Common Data Factory opgeschoonde datasets voor het aanbieden en
beheren van data / monitoring en beleidsinformatie. Voor de data bieden we zowel
de ruwe data, api en een referentie implementatie(s) voor beleidsmedewerkers

Veel inspiratie komt van [Datapunt](https://data.amsterdam.nl).

Data gedreven werken is niet eenvoudig. De meeste beschikbare open data bronnen dienen
verder verwerkt te worden zodat mensen er werkelijk mee aan de slag kunnen.

Gemeentes delen dezelfde taken en data behoeften het is dan ook handig samen te werken aan
open en transparante oplossing die door elke gemeente (her)gebruikt en aangepast kan worden.

Kijk bijvoorbeeld naar [Ondergrond](https://gitlab.com/commondatafactory/ondergrond)
om iets te kunnen zeggen over de beschikbare ruimte onder de grond dient een grote
berg data van verschillende bronnen samengevoegd te worden. Voor elke gemeente zijn daar
weer uitzonderingen op of bijzondere ondergrondse infrastructuur. Door het open en transparant
te beschikbaar maken van het `recept` kunnen mensen er mee aan de slag.

## De standaard

Commondatafactory volgt een aantal principes:

- Herbruikbaarheid
- Opensource
- Transparantie
- Inzicht in datakwaliteit
- Inzicht in bronnen
- Eenvoudig te gebruiken API's
- We toetsen en herbruiken actief bestaande open source oplossingen

## Ons publiek

- Data scientists
- Beleidsmedewerkers
- Burgers
- Adviesbureaus
- Engineers


## De referentie implementatie

De referentie implementatie maakt het eenvoudiger om met de data aan de slag te
gaan. De implementaties zijn te vinden op [commondatafactory.nl](https://commondatafactory.nl) en
alle code is te bekijken  op
[GitLab](https://gitlab.com/commondatafactory). De implementaties worden
onder andere gebruikt om verschillende teams VNG Realisatie en gemeenten te voorzien
van een effectieve datavoorziening.

We werken nauw samen met de mensen van [commonground](https://commonground.nl)
en volgen de [standar for public code](https://standard.publiccode.net)
