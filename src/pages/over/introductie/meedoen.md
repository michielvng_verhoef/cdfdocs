---
title: "Meedoen"
path: "/over/meetings"
---

Om op de hoogte te blijven van de voortgang
en terugkoppeling tegeven aan de developers
van de commondatafactory kunt u zich aanmelden bij verschillende onderdelen:

* online meepraten kan op de [mattermost digilab datafactory](https://digilab.overheid.nl/chat) in het kanaal #datafactory

* algemente vragen vip@vng.nl (het hele data team)

* voor DEGO zijn er maandelijkse (online) data spreekuren. Aanmelden kan bij maaike.liem@vng.nl

* informatie over DOOK abel.koppert@vng.nl

* Kwartaal updates:

Wij proberen elk kwartaal een update te geven over DOOK en DEGO. Hierin wordt gesproken over de voortgang, demo's en ophalen van feedback. Hierbij bent u van harte welkom aan te sluiten.
Voor uitnodigingen en informatie kunt u contact opnemen met  Maaike.Liem@vng.nl
