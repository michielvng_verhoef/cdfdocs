---
title: "Voorbereiding"
path: "/docs/voorbereiding"
---

Voordat je aan de slag kunt gaan met data is er kennis nodig over de data.

## Data eigenschappen

Elke data heeft een aantal belangrijke meta-eigenschappen

1. Resolutie
1. Vernieuwingsfrequentie
1. Eigenaar
1. Kwaliteit
1. Formaat
1. Privacy- / Toegangsbeheer
1. Doelbinding gebruik
1. Waar meld ik fouten?
1. Combinatie van datasets

## Een Dashboard

Een dashboard geeft je toegang tot een grote verzameling data

1. ..
1. ..
