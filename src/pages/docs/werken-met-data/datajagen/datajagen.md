---
title: "Datajagen"
path: "/docs/datajagen"
---

Voordat je aan de slag kunt gaan is er data nodig.
Diverse databronnen die nodig zijn voor werkzaamheden zijn niet
direct in een werkbare vorm voorhanden.

Denk aan planningen, capaciteit van netbeheerders en
verbouwingsplannen van woningcoorporaties.

Mark Knops is onze contactpersoon hiervoor

mark.knops@vng.nl

## Data Jagen

De actuele status van data die we aan het najagens zijn is
op [gitlab.com/dataontsluiting](https://gitlab.com/commondatafactory/dataontsluiting/-/boards)
te vinden.

In onze "Database dumps"-pagina vindt u een paar door ons verwerkte
en gemaakte databronnen die een deel van onderstaande datasets hebben
gebruikt.

Een greep uit de datasets die we op het oog hebben:

### Databron / onderwerp

- AEDES (woning coorporatie / verbouwingen)
- Aansluitingen per verblijfsobject
- Aansluitingen warmtenet
- Aantal aansluitingen gas  PC6
- Bestand bodemgebruik 2015 NGR
- Bestuurlijke grenzen - provincies
- Bestuurlijke grenzen - wijken
- Blokverwarming op pandniveau
- Bouwjaar
- CBS
- CBS kerncijfers samenstelling huishoudens 2018 Buurt
- CO2 uitstoot per buurt
- Capaciteit warmtenet
- Capaciteit, ligging en transport warmtebron
- Data.Overheid.nl Gasnetbeheerdervlak
- De (all electric) elektriciteitsvraag van een wijk is op basis van slecht geïsoleerde woningen en goed geïsoleerde woningen
- Eigendomssituatie
- Energielabel definitief
- Energielabel voorlopig
- Enexis leidingbeheer (combi WFS gasvervanging en gasvervanging_2)
- Functie van een pand
- Gasverbruik, en dit afzetten tegen volume / oppervlak
- Gebouw hoogte (P)
- Gebouwtype
- Gemeentelijk vastgoed
- Geothermiekansen in Noord-Holland
- Geozet bekendmakingen Punten
- Gezinsgrootte
- Handelsregister SBI-codes
- Hittestress Klimaateffectatlas
- Inhoud in m3
- Inkomen
- Investeringsplannen commerciele verhuurders
- Investeringsplanning woningcoorporaties
- Isolatiewaarde, type bedrijfsvoering en bijbehorende warmtevraag van bedrijfspanden.
- Kadaster
- Kosten aanleg van distributie-/transportnetten
- Leefbaarometer buurten 2018
- Leeftijd bewoners
- Leeftijd elektriciteitsnet
- Leeftijd gasnet
- Ligging elektriciteitsnet
- Ligging gasnet
- Ligging huidig warmtenet
- Ligging riolering
- Ligging waterleiding
- Ligging zonnepanelen.
- Locatie onderstations
- Lokale initiatieven
- Meerjarenplanning onderhoud A-/N-wegen 2017
- Meerjarenplanning verhardingsonderhoud NIS jaar van de deklaagplanning groot onderhoud 2017
- Meerjarenplanning verhardingsonderhoud NIS jaar van de deklaagplanning levensverlengend onderhoud 2017
- Meerjarenplanning verhardingsonderhoud NIS jaar van de verhardingstechnische planning groot onderhoud 2017
- Meerjarenplanning verhardingsonderhoud NIS jaar van de verhardingstechnische planning levensverlengend onderhoud 2017
- Natura2000
- Omgevingswarmte 2014
- Onderhoudsplanning water
- Opendata - assets - leveranciers.
- Opleidingsniveau
- Overige infra ondergrond
- Postcode-6 gebieden
- Panden met collectieve gasaansluiting
- Plannen nieuwbouw en sloop
- Plannen riolering
- Plannen voor WKO
- Plannen warmtenet
- Pnh dataservice alg: Woningbouwplannen
- Restcapaciteit e-net
- Risico energiearmoede
- Stedin Leidingbeheer 20190517
- Temperatuur warmtenet
- Teruglevering energie electra
- Teruglevering zonne-energie (PV)
- Transport of afleverbuis
- Type aardgasaansluiting
- Verbod WKO door bescherming drinkwater
- Verbod WKO door specifiek provinciaal beleid
- Verbruiksdata elektriciteit en gas per SBI code
- Verbruiksdata elektriciteit 2014-2019 (PC6)
- Verbruiksdata gas 2014-2019 (PC6)
- Verbruiksdata elektriciteit 2014
- Verbruiksdata elektriciteit 2015
- Verbruiksdata elektriciteit 2016
- Verbruiksdata elektriciteit 2017
- Verbruiksdata elektriciteit 2018
- Verbruiksdata gas 2014
- Verbruiksdata gas 2015
- Verbruiksdata gas 2016
- Verbruiksdata gas 2017
- Verbruiksdata gas 2018
- Vermogen cascadering-bron en afstand tot de buurten
- Vervangingsplannen elektriciteit
- Vervangingsplannen gas
- Verwachte elektriciteitstoename
- Vloeroppervlakte
- WKO-diep geschiktheid in Noord-Holland
- WKO-ondiep geschiktheid in Noord-Holland
- Warmteprijzen op wijk- en buurtniveau
- Warmtevraag utiliteit
- Wateroverlast Klimaateffect atlas
- Westland leidingbeheer
- WijkEnBuurten - Buurten 2018
- WijkEnBuurten - Wijken 2018
- Wijkvernieuwingen
