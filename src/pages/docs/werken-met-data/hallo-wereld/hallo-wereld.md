---
title: "Hallo wereld"
path: "/docs/hallo-wereld"
---

In deze sectie moeten we nog veel meer informatie zetten en is nog niet af.

![Schermafbeelding van de applicatie](./hallo-wereld.png)

## Broncode en Docker images

De broncode van de applicatie is te vinden op
[gitlab](https://www.gitlab.com/commondatafactory).
