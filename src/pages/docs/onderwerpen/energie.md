---
title: "Energietransitie"
path: "/docs/energie"
---

### Energietransitie is de grootste verbouwing voor Nederland.

De energietransitie is een project dat alle Nederlandse
gemeenten betreft: volgens het klimaatakkoord moet elke
gemeente in 2021 een plan hebben opgesteld welke buurt wanneer
van het aardgas zal worden gehaald.


En gemeenten zijn verantwoordelijk voor de uitvoering
en monitoring van hun plannen.

### Betaalbaar een transitie naar duurzame warmte bronnen

Om de gemeenten hierin te ondersteunen heeft VNG Realisatie de
opdracht op zich genomen om te helpen bij het
verkijgen van informatie, bouwen van APIs en maken van kaarten.
Hiermee kunnen alle gemeenten ondersteund worden bij het opzetten
van een energietransitie-plan.

Voor een plan dient een uitgebreide datagedreven afweging
gemaakt te worden tussen veel belangen en factoren, die bovendien
uitlegbaar moeten zijn voor de gewone burger.

We verzamelen en toetsen data en zorgen dat gemeenten een
start kunnen maken in de uitvoering van hun plannen en zelf een goede afweging kunnen maken

We zijn actief aan het datajagen:

[Datasets](/docs/datajagen)

### Help mee
We zijn actief op zoek naar gemeenten die deel willen nemen, de producten
gebruiken en feedback geven over de ontwikkeling van deze
hulpmiddelen.

Er zijn nu meerdere meetups, waaronder elke twee weken een review, en we
helpen graag verder met vragen en suggesties. Neem contact op met Paul Suijkerbuijk / Maaike Liem [contact gegevens](/contact)

Samen met gemeenten, software engineers, experts, cartografen
en andere energietransitie-initiatieven werken we aan deze voorziening.

Door middel van data-beschikbaarheid, vragen, gemeentelijke taken, en engineering
capaciteit proberen we zo effectief mogelijk het datagedreven werken
te bevorderen.


## Kaarten

[Transitie visie warmteviewer](https://tvw.commondatafactory.nl)

[Energie data viewer](https://energie.commondatafactory.nl)

### QGIS

DEGO data gelijk in Qgis.

[kant-en-klaar qgis project](https://files.commondatafactory.nl/qgis/DEGO-2023-08.qgz)

### energie data per gebouw WFS

[acc.maps.commondatafactory.nl/maps/energy](https://acc.maps.commondatafactory.nl/maps/energy)



### Wat voor plannen zijn er al?

Bestaande plannen komen ook beschikbaar om te kijken naar de aanpak
van andere gemeenten.

[Contact gegevens](/contact) Paul Suijkerbuijk / Maaike Liem


## Koppelkansen en afwegingen

Een greep uit factoren die meewegen en die potentieel een plek krijgen
in de viewer(s) / database / api zijn:

 - Wijkvernieuwingsplannen
 - Voorspellingen uit modellen (Vesta Mais)
 - Rioolplanning
 - Afschrijving bestaande gasleidingen
 - Aanpassingen om tegen klimaat verandering te kunnen
 - Aanpassingen tegen wateroverlast
 - Aanpassingen tegen warmte
 - Mogelijkheid tot collectieve verwarming
 - Bestaande buurtinitatieven
 - Ruimte in de omgeveving
 - WOZ-waarde in buurten
 - Beschikbaarheid van rest-energie
 - Leeftijd van bebouwing
 - Capaciteit van bestaande elektra-net
 - Planning
 - Teruglevering van zonnepanelen
 - Verdeling van groot-eigenaars zoals woning coorporaties
