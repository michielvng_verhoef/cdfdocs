---
title: "Ondermijning"
path: "/docs/ondermijning"
---

### Help mee
We zijn actief op zoek naar gemeenten die deel willen nemen, de producten
gebruiken en feedback geven over de ontwikkeling van deze
hulpmiddelen.

Er zijn nu meerdere meetups, waaronder elke twee weken een review, en we
helpen graag verder met vragen en suggesties. Neem contact op met Paul Suijkerbuijk [contact gegevens](/contact)

Samen met gemeenten, software engineers, experts, cartografen
en andere energietransitie-initiatieven werken we aan deze voorziening.

Door middel van data-beschikbaarheid, vragen, gemeentelijke taken, en engineering
capaciteit proberen we zo effectief mogelijk het datagedreven werken
te bevorderen.


## Kaarten


