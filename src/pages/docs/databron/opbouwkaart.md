---
title: "Opbouw van de Kaart"
path: "/docs/opbouwkaart"
---


## Broncode en Docker images

Alle broncode van services en applicatie(s) zijn te vinden op
[gitlab](https://www.gitlab.com/commondatafactory).


## Uitleg kaart en data lagen

### 1) Basis is Open Streetmap
De kaarten worden gemaakt met behulp van zogenaamde
vector-tiles. Een relatief nieuwe kaart-techniek
die toestaat om veel data flexibel een "kaartstijl"
te geven en data op kaart speciaal naar toepassing te veranderen.

Als basis ondergrond gebruiken we Openstreetmap (OSM) kaart.
In de OSM staan veel extra gegevens over bedrijvigheid en voetpaden
winkels, openbaarvervoer die vaak in andere kaart registraties missen.
OSM is dus de basis.

### 2) BAG + Combinatie van gegevens.
De kleuren die gebouwen krijgen als er op verschillende kaartlagen
wordt geselecteerd is een **zelfgemaakte** kaartlaag.
De basis van deze gewbouwen kaartlaag is de BAG. (Basis registratrie Adressen en Gebouwen).
Toegevoegd aan de gebouwen is de BAG3D dataset gemaakt door de TU delft die
volume en hoogte gegevens toevoegd aan gewbouwen. Deze gegevens worden verzameld
met behulp van laser data metingen. Met vliegtuig of laser.

- BAG
- BAG3D
- CBS
- RVO
- Kleinverbruiksgegevens
- KVK

Van deze Bronnen combineren we met panden en geo - objecten om een effectieve kaart te maken geschikt
voor uitvoeren van taken. Het effectief en correct combineren is het werk van de Common Data Factory.

## Data lagen in DEGO / DOOK

Verzamelingen van de verschillende zelfgemaakte kaartlagen speciaal in overleg met VNG en gemeenten samengesteld.


### Buurten, wijken en gemeenten

Als basis voor de administratieve grenzen wordt gebruik gemaakt van de CBS indeling. Zie [wijk en buurtkaart ](https://www.cbs.nl/nl-nl/dossier/nederland-regionaal/geografische-data) .

Het jaartal van de gepubliceerde data van het CBS zal sterk verschillen per attribuut die wordt getoont. Omdat CBS cijfers vaak later aanleverd kunnen attributen beschikbaar zijn uit verschillende jaren.
Let goed op bij elke laag uit welk jaar de data komt.

De data wordt bij ons getoont op de grenzen van het jaar waarvan de data is.


##  API
Een API is een Application Programming Interface. De toegangs poorten voor de viewers en andere data gebruikers. Een API gebruiken is handig. De API voorziening is verantwoordelijk voor het up-to-date aanbieden van de data. API zijn er in vele soorten en maten.  We zullen op termijn zoveel mogelijk datasets aanbieden als API en als ruwe data.

U kunt hier de data beschrijving van de API's vinden die wij ontsluiten en aanbieden. API's met gevoelige data komen alleen via NLX beschikbaar.

Wat wij aanbieden:

- verschillende WFS services
- REST API's voor dataselecties
- Onze vector-tiles


## CSV downloads

Naast de data lagen op de kaart, zijn er ook verschillende downloads en API's te vinden in onze applicaties.
[csv](/docs/csv)


## Update cyclus

Het koppelen van de gegevens is bijzonder lastig veel datasets werken met
postcodes of adressen die niet altijd eenvoudig aan de betrokken gebouwen
gekoppeld kan worden.

We proberen de verschillende bronnen actueel te houden
maar door de wisselende data structuren en wisselende vernieuwingscycli

In de toekomst gaan wij ons focussen op het automatiseren van de updates.
Waardoor er steeds meer en vaker up-to-date data beschikbaar zal zijn!
