---
title: "Data Documentatie"
path: "/docs"
---

## De standaard

Commondatafactory volgt een aantal principes:

- Herbruikbaarheid
- Opensource
- Transparantie
- Inzicht in datakwaliteit
- Inzicht in bronnen
- Eenvoudig te gebruiken API's
- We toetsen en herbruiken actief bestaande open source oplossingen

## De referentie implementatie

De referentie implementatie maakt het eenvoudiger om met de data aan de slag te
gaan. De implementaties zijn te vinden op [commondatafactory.nl](https://commondatafactory.nl) en
alle code is te bekijken  op
[GitLab](https://gitlab.com/commondatafactory). De implementaties worden
onder andere gebruikt om verschillende teams VNG Realisatie en gemeenten te voorzien
van een effectieve datavoorziening.

We werken nauw samen met de mensen van [commonground](https://commonground.nl)
en volgen de [standar for public code](https://standard.publiccode.net)
