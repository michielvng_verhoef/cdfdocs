module.exports = {
  siteMetadata: {
    title: `CommonDataFactory`,
    description: `Datasets / Kaarten / API's / Apps voor (her)gebruik bij uitvoering, voorlichting en plannenmakerij`,
    author: `VNG Realisatie`,
    repositoryUrl: `https://www.gitlab.com/commondatafactory/docs/cdf`,
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },


    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-check-links`,
          `gatsby-remark-prismjs`,
          `gatsby-remark-images`,
        ]
      }
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
      rule: {
	include: /svg/
      }
    }
   },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `CommonDataFactory`,
        short_name: `CDF`,
        start_url: `/`,
        display: `standalone`,
        icon: `src/images/cdflogo.png`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
